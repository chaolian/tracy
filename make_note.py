#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: clian
"""

import numpy as np
import argparse
import os
import json

template_head = open('/home/clian/Desktop/note/Tracy/template_head.tex').read()
template_end=r'''
\bibliography{/home/clian/Desktop/note/ref}
\clearpage\end{CJK}

\end{document}
!
'''

from datetime import date, datetime

class options(argparse.ArgumentParser):
    def __init__(self):
          super(options, self).__init__()
          self.add_argument('-f', '--file', type=str, nargs='?', 
                            default = str(date.today())+'.tex',
                            help='filename of note')
          self.add_argument('-n', '--new', type=str, nargs='?', 
                            default = 'y', 
                            help='create a new note')
          self.add_argument('-u', '--update', type=str, nargs='?', 
                            default = 'y', 
                            help='update the full note')
          self.args = self.parse_args()

options = options()
args = options.args

#--------------------------------------------------------------------------------------
if 'y' or 'Y' in args.new:
    line = '\import{./}{%s}'%args.file
    temp = template_head + line + template_end
    
    if not os.path.exists(args.file):
        open(args.file, 'w').write('Empty  \n')
    
    wrapper_name = 'wrapper_%s'%args.file
    open(wrapper_name,'w').write(temp)
    
    prefix='.'.join(args.file.split('.')[:-1])
    #os.system('pdflatex -jobname=%s %s'%(prefix, wrapper_name))
    
#--------------------------------------------------------------------------------------
if 'y' or 'Y' in args.update:
    db_file = "/home/clian/Desktop/note/Tracy/note_tex.json"
    with open(db_file, "r") as read_file:
        data = json.load(read_file)
        #print(data)
    
    fullname = os.path.abspath('.') + '/%s'%args.file
    file_create_date = os.path.getctime(args.file)
    
    formated_date = datetime.fromtimestamp(file_create_date).strftime('%Y-%m-%d %H:%M:%S')
    if fullname not in data.keys():
        data[fullname] = file_create_date
        
    #print(data)
    
    with open(db_file, "w") as write_file:
        write_file.write(json.dumps(data))
        #print(data)
    
    #print(sorted(data))
    temp = ''
    for file in sorted(data):
        path_dict = file.split('/')
        path = '/'.join(path_dict[:-1])
        filename = path_dict[-1]
        
        line = '\n%s\n%s\n\\import{%s}{%s}\n\\clearpage\n'%(formated_date, file, path, filename)
        temp = temp + line
    
    settings = '\n\\tableofcontents\n\clearpage\n'
    open('/home/clian/Desktop/note/Tracy/full_note.tex','w').write(template_head + settings + temp + template_end)
    #os.system('pdflatex /home/clian/Desktop/note/Tracy/full_note.tex')
    #print(line)
#for file in dict(sorted(x.items(), key=lambda item: item[1]))


# db_file = open('note_tex.db','r')
# note_db = json.loads(db_file)
# print(note_db)
#print(args)





























